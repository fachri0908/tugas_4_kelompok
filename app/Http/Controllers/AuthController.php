<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller {
    public function index() {
        return view('pages.login');
    }  
      
    public function authenticate(Request $request) {

        $user = User::where('username', $request->username)->where('password', $request->password)->first();
        if ($user) {
            Auth::login($user);
            return redirect('/');
        } 
  
        // return redirect("login")->withErrors('Login details are not valid');
    }
    
    public function signOut() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
}