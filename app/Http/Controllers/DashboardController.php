<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Jawaban;
use App\Models\Dimensi;
use DB;

class DashboardController extends Controller {
    public function index() {
        return view('pages.home.index', [
            'user' => Auth::user(),
            'jawaban1' => DB::select("SELECT jawaban,
                CASE jawaban
                WHEN 'A' THEN Count(jawaban)*1 
                WHEN 'B' THEN COUNT(jawaban)*2 
                WHEN 'C' THEN COUNT(jawaban)*3 
                WHEN 'D' THEN COUNT(jawaban)*4 
                WHEN 'E' THEN COUNT(jawaban)*5 
                END as jumlah
                FROM jawaban,tbkuesioner
                WHERE (tbkuesioner.id_kuesioner=jawaban.id_kuesioner) AND
                (tbkuesioner.id_dimensi=1)
                GROUP BY jawaban"),
            'jawaban2' => DB::select("SELECT jawaban,
                CASE jawaban
                WHEN 'A' THEN Count(jawaban)*1 
                WHEN 'B' THEN COUNT(jawaban)*2 
                WHEN 'C' THEN COUNT(jawaban)*3 
                WHEN 'D' THEN COUNT(jawaban)*4 
                WHEN 'E' THEN COUNT(jawaban)*5 
                END as jumlah
                FROM jawaban,tbkuesioner
                WHERE (tbkuesioner.id_kuesioner=jawaban.id_kuesioner) AND
                (tbkuesioner.id_dimensi=2)
                GROUP BY jawaban"),
            'jawaban3' => DB::select("SELECT jawaban,
                CASE jawaban
                WHEN 'A' THEN Count(jawaban)*1 
                WHEN 'B' THEN COUNT(jawaban)*2 
                WHEN 'C' THEN COUNT(jawaban)*3 
                WHEN 'D' THEN COUNT(jawaban)*4 
                WHEN 'E' THEN COUNT(jawaban)*5 
                END as jumlah
                FROM jawaban,tbkuesioner
                WHERE (tbkuesioner.id_kuesioner=jawaban.id_kuesioner) AND
                (tbkuesioner.id_dimensi=3)
                GROUP BY jawaban"),
            'jawaban4' => DB::select("SELECT jawaban,
                CASE jawaban
                WHEN 'A' THEN Count(jawaban)*1 
                WHEN 'B' THEN COUNT(jawaban)*2 
                WHEN 'C' THEN COUNT(jawaban)*3 
                WHEN 'D' THEN COUNT(jawaban)*4 
                WHEN 'E' THEN COUNT(jawaban)*5 
                END as jumlah
                FROM jawaban,tbkuesioner
                WHERE (tbkuesioner.id_kuesioner=jawaban.id_kuesioner) AND
                (tbkuesioner.id_dimensi=4)
                GROUP BY jawaban"),
            'dimensi' => Dimensi::get(),
            'dimensi1' => Dimensi::find(1),
            'dimensi2' => Dimensi::find(2),
            'dimensi3' => Dimensi::find(3),
            'dimensi4' => Dimensi::find(4),
        ]);

    }
}
