<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Dimensi;

class DimensiController extends Controller {
    public function index() {
        return view('pages.dimensi.index', [
            'user' => Auth::user(),
            'dimensis' => Dimensi::get(),
        ]);
    }

    public function add(Request $request) {
        return view('pages.dimensi.add', [
            'user' => Auth::user()
        ]);
    }

    public function insert(Request $request) {
        $dimensi = new Dimensi;
        $dimensi->dimensi = $request->dimensi;
        $dimensi->bobot = $request->bobot;
        $dimensi->save();
        return redirect('dimensi');
    }

    public function edit(Request $request) {
        return view('pages.dimensi.edit', [
            'user' => Auth::user(),
            'dimensi' => Dimensi::find($request->id_dimensi)
        ]);
    }
    
    public function update(Request $request) {
        $dimensi = Dimensi::find($request->id_dimensi);
        $dimensi->dimensi = $request->dimensi;
        $dimensi->bobot = $request->bobot;
        $dimensi->save();
        return redirect('dimensi');
    }

    public function delete(Request $request) {
        $dimensi = Dimensi::find($request->id_dimensi);
        $dimensi->delete();
        return redirect('dimensi');
    }
}
