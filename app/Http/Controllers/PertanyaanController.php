<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Kuesioner;
use App\Models\Dimensi;

class PertanyaanController extends Controller {
    public function index() {
        return view('pages.pertanyaan.index', [
            'user' => Auth::user(),
            'questions' => Kuesioner::get(),
        ]);
    }

    public function add(Request $request) {
        return view('pages.pertanyaan.add', [
            'user' => Auth::user(),
            'dimensions' => Dimensi::get()
        ]);
    }

    public function insert(Request $request) {
        $pertanyaan = new Kuesioner;
        $pertanyaan->pertanyaan = $request->pertanyaan;
        $pertanyaan->id_dimensi = $request->id_dimensi;
        $pertanyaan->variabel = $request->variabel;
        $pertanyaan->pila = $request->pila;
        $pertanyaan->pilb = $request->pilb;
        $pertanyaan->pilc = $request->pilc;
        $pertanyaan->pild = $request->pild;
        $pertanyaan->pile = $request->pile;
        $pertanyaan->save();
        return redirect('pertanyaan');
    }

    public function edit(Request $request) {
        return view('pages.pertanyaan.edit', [
            'user' => Auth::user(),
            'pertanyaan' => Kuesioner::find($request->id_pertanyaan),
            'dimensions' => Dimensi::get(),
        ]);
    }
    
    public function update(Request $request) {
        $pertanyaan = Kuesioner::find($request->id_pertanyaan);
        $pertanyaan->pertanyaan = $request->pertanyaan;
        $pertanyaan->id_dimensi = $request->id_dimensi;
        $pertanyaan->variabel = $request->variabel;
        $pertanyaan->pila = $request->pila;
        $pertanyaan->pilb = $request->pilb;
        $pertanyaan->pilc = $request->pilc;
        $pertanyaan->pild = $request->pild;
        $pertanyaan->pile = $request->pile;
        $pertanyaan->save();
        return redirect('pertanyaan');
    }

    public function delete(Request $request) {
        $pertanyaan = Kuesioner::find($request->id_pertanyaan);
        $pertanyaan->delete();
        return redirect('pertanyaan');
    }
}
