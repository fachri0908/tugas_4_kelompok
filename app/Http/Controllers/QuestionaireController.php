<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Kuesioner;
use App\Models\Dimensi;
use App\Models\Jawaban;

class QuestionaireController extends Controller {
    public function index() {
        return view('pages.questionaire.index', [
            'user' => Auth::user(),
            'questions' => Kuesioner::get(),
        ]);
    }

    public function submitQuestionaire(Request $request) {
        $questions = Kuesioner::get();
        if(count($questions) != count($request->pilihan)) {
            return redirect('questionaire')->with('submittedData', $request->pilihan);
        }
        foreach($questions as $question) {
            $jawaban = new Jawaban;
            $jawaban->id_kuesioner = $question->id_kuesioner;
            $jawaban->jawaban = $request->pilihan[$question->id_kuesioner];
            $jawaban->username = Auth::user()->username;
            $jawaban->save();
        }
        return redirect('/');
    }
}
