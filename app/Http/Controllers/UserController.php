<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller {
    public function index() {
        return view('pages.user.index', [
            'user' => Auth::user(),
            'users' => User::get(),
        ]);
    }

    public function add(Request $request) {
        return view('pages.user.add', [
            'user' => Auth::user()
        ]);
    }

    public function insert(Request $request) {
        $user = new User;
        $user->username = $request->username;
        $user->password = $request->password;
        $user->hak_akses = $request->hak_akses;
        $user->save();
        return redirect('user');
    }

    public function edit(Request $request) {
        return view('pages.user.edit', [
            'user' => Auth::user(),
            'selectedUser' => User::find($request->id_user)
        ]);
    }
    
    public function update(Request $request) {
        $user = User::find($request->id_user);
        $user->username = $request->username;
        $user->password = $request->password;
        $user->hak_akses = $request->hak_akses;
        $user->save();
        return redirect('user');
    }

    public function delete(Request $request) {
        $user = User::find($request->id_user);
        $user->delete();
        return redirect('user');
    }
}
