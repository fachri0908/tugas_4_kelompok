<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuesioner extends Model {
    use HasFactory;
    protected $table = "tbkuesioner";
    protected $primaryKey = "id_kuesioner";
    public $timestamps = false;

    protected $fillable = [
        'id_kuesioner',
        'pertanyaan',
        'id_dimensi',
        'variabel',
        'pila',
        'pilb',
        'pilc',
        'pild',
        'pile',
    ];
}
