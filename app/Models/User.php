<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = "tbpengguna";
    protected $primaryKey = "user_id";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'username',
        'password',
        'hak_akses'
    ];

    protected $hidden = [
        'password',
    ];
}
