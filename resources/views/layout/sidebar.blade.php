<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="{{route('dashboard')}}"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
            <li>
                <a class="dropmenu"><i class="icon-envelope"></i><span class="hidden-tablet"> Dimensi</a>
                <ul>
                    <li><a class="submenu" href="{{route('dimensi.index')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{route('dimensi.add')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu"><i class="icon-tasks"></i><span class="hidden-tablet"> Pertanyaan</a>
                <ul>
                    <li><a class="submenu" href="{{route('pertanyaan.index')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{route('pertanyaan.add')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>	
            </li>
            <li>
                <a class="dropmenu"><i class="icon-folder-open"></i><span class="hidden-tablet"> Pengguna</a>
                <ul>
                    <li><a class="submenu" href="{{route('user.index')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{route('user.add')}}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>	
            </li>
            <li><a href="{{route('questionaire.index')}}"><i class="icon-edit"></i><span class="hidden-tablet"> Jawab Kuesioner</span></a></li>
            
        </ul>
    </div>
</div>