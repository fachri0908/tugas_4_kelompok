@extends('layout.master')
@section('content')
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white edit"></i><span class="break"></span>Tambah Data Dimensi</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="POST" action="{{route('dimensi.update', $dimensi->id_dimensi)}}">
					@csrf
					<fieldset>
						<div class="control-group">
						<label class="control-label" for="focusedInput">Dimensi : </label>
						<div class="controls">
							<input class="input-xlarge focused" id="focusedInput" type="text" value="{{$dimensi->dimensi}}" name="dimensi">
						</div>
						</div>
						<div class="control-group">
						<label class="control-label" for="focusedInput">Bobot : </label>
						<div class="controls">
							<input class="input-xlarge focused" id="" type="text" value="{{$dimensi->bobot}}" name="bobot">
						</div>
						</div>

						<div class="form-actions">
						<button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
						<button class="btn">Cancel</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div><!--/span-->
	</div><!--/row-->
@endsection