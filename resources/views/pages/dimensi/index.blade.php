@extends('layout.master')
@section('content')
	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Dimensi</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Dimensi</th>
						<th>Bobot</th>
						<th>Actions</th>
					</tr>
				</thead>   
				<tbody>
					@foreach($dimensis as $dimensi)
					<tr>
						<td class="center">{{$loop->iteration}}</td>
						<td>{{$dimensi->dimensi}}</td>
						<td class="center">{{$dimensi->bobot}}</td>
						
						<td class="center">
							<a class="btn btn-info" href="{{route('dimensi.edit', $dimensi->id_dimensi)}}">
								<i class="halflings-icon white edit"></i>  
							</a>
							<a class="btn btn-danger" href="{{route('dimensi.delete', $dimensi->id_dimensi)}}">
								<i class="halflings-icon white trash"></i> 
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>            
			</div>
		</div>
	</div>
@endsection