@extends('layout.master')
@section('content')
<div class="row-fluid">
    <div class="widget red span3" onTablet="span4" onDesktop="span3">
        <h3><span class="glyphicons globe"><i></i></span>CORPORATE CONTRIBUTION</h3>
        <hr>
        <div class="content">
            <div class="verticalChart">
                <?php
                    $urut=0;
                    $total=0;
                    $kategori="";
                    foreach($jawaban1 as $jawaban)
                    {
                        $urut=$urut+1;
                        $option=$jawaban->jawaban;
                        $jumlah=$jawaban->jumlah;
        
                        $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                        $total=$total+$jumlah;
                    }
                            
                ?>

                <div class="singleBar">
                
                    <div class="bar">
                    
                        <div class="value">
                            <span>{{$jumlah}}</span>
                        </div>
                    
                    </div>
                    
                    <div class="title">{{$option}}</div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
        
        </div>
        
    </div>
    
    <div class="widget blue span3" onTablet="span4" onDesktop="span3">
        
        <h3><span class="glyphicons magic"><i></i></span>STAKEHOLDER ORIENTATION</h3>
        
        <hr>
        
        <div class="content">
            
            <div class="verticalChart">
                <?php
                    $urut=0;
                    $total=0;
                    $kategori="";
                    foreach($jawaban2 as $jawaban)
                    {
                        $urut=$urut+1;
                        $option=$jawaban->jawaban;
                        $jumlah=$jawaban->jumlah;
        
                        $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                        $total=$total+$jumlah;
                    }
                            
                ?>

                <div class="singleBar">
                
                    <div class="bar">
                    
                        <div class="value">
                            <span>{{$jumlah}}</span>
                        </div>
                    
                    </div>
                    
                    <div class="title">{{$option}}</div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
        
        </div>
        
    </div>
    
    <div class="widget yellow span3" onTablet="span4" onDesktop="span3">
        
        <h3><span class="glyphicons pie_chart"><i></i></span>OPERATIONAL EXCELLENCE</h3>
        
        <hr>
        
        <div class="content">
            
            <div class="verticalChart">
                <?php
                    $urut=0;
                    $total=0;
                    $kategori="";
                    foreach($jawaban3 as $jawaban)
                    {
                        $urut=$urut+1;
                        $option=$jawaban->jawaban;
                        $jumlah=$jawaban->jumlah;
        
                        $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                        $total=$total+$jumlah;
                    }
                            
                ?>

                <div class="singleBar">
                
                    <div class="bar">
                    
                        <div class="value">
                            <span>{{$jumlah}}</span>
                        </div>
                    
                    </div>
                    
                    <div class="title">{{$option}}</div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
        
        </div>
        
    </div>

    <div class="widget green span3" onTablet="span4" onDesktop="span3">
        
        <h3><span class="glyphicons snowflake"><i></i></span>FUTURE ORIENTATION</h3>
        
        <hr>
        
        <div class="content">
            
            <div class="verticalChart">
                <?php
                    $urut=0;
                    $total=0;
                    $kategori="";
                    foreach($jawaban4 as $jawaban)
                    {
                        $urut=$urut+1;
                        $option=$jawaban->jawaban;
                        $jumlah=$jawaban->jumlah;
        
                        $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                        $total=$total+$jumlah;
                    }
                            
                ?>

                <div class="singleBar">
                
                    <div class="bar">
                    
                        <div class="value">
                            <span>{{$jumlah}}</span>
                        </div>
                    
                    </div>
                    
                    <div class="title">{{$option}}</div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
        
        </div>
        
    </div>

<div class="row-fluid hideInIE8 circleStats">
    
    <div class="span3" onTablet="span4" onDesktop="span3">
        <div class="circleStatsItemBox yellow">
            <div class="header">CORPORATE CONTRIBUTION</div>
            <span class="percent">percent</span>
            <?php
                $urut=0;
                $total=0;
                $kategori="";
                foreach($jawaban1 as $jawaban)
                {
                    $urut=$urut+1;
                    $option=$jawaban->jawaban;
                    $jumlah=$jawaban->jumlah;
    
                    $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                    $total=$total+$jumlah;
                }
                if ($total<=48.6) {
                    $kategori="Sangat Buruk";
                    $bobot_kinerja=1;
                } elseif (($total>=48.7) && ($total<=70.2)) {
                    $kategori="Buruk";
                    $bobot_kinerja=2;
                } elseif (($total>=70.3) && ($total<=91.8)) {
                    $kategori="Cukup Baik";
                    $bobot_kinerja=3;
                } elseif (($total>=91.9) && ($total<=113.4)) {
                    $kategori="Baik";
                    $bobot_kinerja=4;
                } else {
                    $kategori="Sangat Baik";
                    $bobot_kinerja=5;
                }

                $KPI=($bobot_kinerja/$dimensi1->bobot)*100;
                //kesimpulan
                if (($KPI>=1) && ($KPI<=20)) {
                    $kategori_kpi="Tidak Baik";
                } elseif (($KPI>=21) && ($KPI<=40)) {
                    $kategori_kpi="Kurang";
                } elseif (($KPI>=41) && ($KPI<=60)) {
                    $kategori_kpi="Cukup";
                } elseif (($KPI>=61) && ($KPI<=80)) {
                    $kategori_kpi="Baik";
                } else {
                    $kategori_kpi="Sangat Baik";
                }
                $kesimpulan="Kesimpulan KPI adalah " . $kategori_kpi;	
                        
            ?>
            <div class="circleStat">
                <input type="text" value="{{$KPI}}" class="whiteCircle" />
            </div>		
            <div class="footer">
                <span class="count">
                    <span class="unit">{{$kesimpulan}}</span>
                    
                </span>
                
            </div>
        </div>
    </div>

    <div class="span3" onTablet="span4" onDesktop="span3">
        <div class="circleStatsItemBox green">
            <div class="header">STAKEHOLDER ORIENTATION</div>
            <span class="percent">percent</span>
            <?php
                $urut=0;
                $total=0;
                $kategori="";
                foreach($jawaban2 as $jawaban)
                {
                    $urut=$urut+1;
                    $option=$jawaban->jawaban;
                    $jumlah=$jawaban->jumlah;
    
                    $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                    $total=$total+$jumlah;
                }
                if ($total<=48.6) {
                    $kategori="Sangat Buruk";
                    $bobot_kinerja=1;
                } elseif (($total>=48.7) && ($total<=70.2)) {
                    $kategori="Buruk";
                    $bobot_kinerja=2;
                } elseif (($total>=70.3) && ($total<=91.8)) {
                    $kategori="Cukup Baik";
                    $bobot_kinerja=3;
                } elseif (($total>=91.9) && ($total<=113.4)) {
                    $kategori="Baik";
                    $bobot_kinerja=4;
                } else {
                    $kategori="Sangat Baik";
                    $bobot_kinerja=5;
                }

                $KPI=($bobot_kinerja/$dimensi2->bobot)*100;
                //kesimpulan
                if ($KPI<=20) {
                    $kategori_kpi="Tidak Baik";
                } elseif (($KPI>=21) && ($KPI<=40)) {
                    $kategori_kpi="Kurang";
                } elseif (($KPI>=41) && ($KPI<=60)) {
                    $kategori_kpi="Cukup";
                } elseif (($KPI>=61) && ($KPI<=80)) {
                    $kategori_kpi="Baik";
                } else {
                    $kategori_kpi="Sangat Baik";
                }
                $kesimpulan="Kesimpulan KPI adalah " . $kategori_kpi;	
                        
            ?>
            <div class="circleStat">
                <input type="text" value="<?php echo $KPI; ?>" class="whiteCircle" />
            </div>
            <div class="footer">
                <span class="count">
                    <span class="unit"><?php echo $kesimpulan; ?></span>
                    
                </span>
                
            </div>
        </div>
    </div>

    <div class="span3" onTablet="span4" onDesktop="span3">
        <div class="circleStatsItemBox red">
            <div class="header">OPERATIONAL EXCELLENCE </div>
            <span class="percent">percent</span>
            <?php
                $urut=0;
                $total=0;
                $kategori="";
                foreach($jawaban3 as $jawaban)
                {
                    $urut=$urut+1;
                    $option=$jawaban->jawaban;
                    $jumlah=$jawaban->jumlah;
    
                    $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                    $total=$total+$jumlah;
                }
                if ($total<=48.6) {
                    $kategori="Sangat Buruk";
                    $bobot_kinerja=1;
                } elseif (($total>=48.7) && ($total<=70.2)) {
                    $kategori="Buruk";
                    $bobot_kinerja=2;
                } elseif (($total>=70.3) && ($total<=91.8)) {
                    $kategori="Cukup Baik";
                    $bobot_kinerja=3;
                } elseif (($total>=91.9) && ($total<=113.4)) {
                    $kategori="Baik";
                    $bobot_kinerja=4;
                } else {
                    $kategori="Sangat Baik";
                    $bobot_kinerja=5;
                }
                $KPI=($bobot_kinerja/$dimensi3->bobot)*100;
                //kesimpulan
                if (($KPI>=0) && ($KPI<=20)) {
                    $kategori_kpi="Tidak Baik";
                } elseif (($KPI>=21) && ($KPI<=40)) {
                    $kategori_kpi="Kurang";
                } elseif (($KPI>=41) && ($KPI<=60)) {
                    $kategori_kpi="Cukup";
                } elseif (($KPI>=61) && ($KPI<=80)) {
                    $kategori_kpi="Baik";
                } else {
                    $kategori_kpi="Sangat Baik";
                }
                $kesimpulan="Kesimpulan KPI adalah " . $kategori_kpi;	
                        
            ?>
            <div class="circleStat">
                <input type="text" value="<?php echo $KPI; ?>" class="whiteCircle" />
            </div>
            <div class="footer">
                <span class="count">
                    <span class="unit"><?php echo $kesimpulan; ?></span>
                </span>
            </div>
        </div>
    </div>

    <div class="span3 noMargin" onTablet="span4" onDesktop="span3">
        <div class="circleStatsItemBox pink">
            <div class="header">FUTURE ORIENTATION</div>
            <span class="percent">percent</span>
            <?php
                $urut=0;
                $total=0;
                $kategori="";
                foreach($jawaban4 as $jawaban)
                {
                    $urut=$urut+1;
                    $option=$jawaban->jawaban;
                    $jumlah=$jawaban->jumlah;
    
                    $stringedit =  str_replace(" ", ",", ($jumlah. " "));
                    $total=$total+$jumlah;
                }
                if (($total>=27) && ($total<=48.6)) {
                    $kategori="Sangat Buruk";
                    $bobot_kinerja=1;
                } elseif (($total>=48.7) && ($total<=70.2)) {
                    $kategori="Buruk";
                    $bobot_kinerja=2;
                } elseif (($total>=70.3) && ($total<=91.8)) {
                    $kategori="Cukup Baik";
                    $bobot_kinerja=3;
                } elseif (($total>=91.9) && ($total<=113.4)) {
                    $kategori="Baik";
                    $bobot_kinerja=4;
                } else {
                    $kategori="Sangat Baik";
                    $bobot_kinerja=5;
                }

                $KPI=($bobot_kinerja/$dimensi4->bobot)*100;
                //kesimpulan
                if (($KPI>=1) && ($KPI<=20)) {
                    $kategori_kpi="Tidak Baik";
                } elseif (($KPI>=21) && ($KPI<=40)) {
                    $kategori_kpi="Kurang";
                } elseif (($KPI>=41) && ($KPI<=60)) {
                    $kategori_kpi="Cukup";
                } elseif (($KPI>=61) && ($KPI<=80)) {
                    $kategori_kpi="Baik";
                } else {
                    $kategori_kpi="Sangat Baik";
                }
                $kesimpulan="Kesimpulan KPI adalah " . $kategori_kpi;	
            ?>
            <div class="circleStat">
                <input type="text" value="<?php echo $KPI; ?>" class="whiteCircle" />
            </div>
            <div class="footer">
    
                <span class="count">
                    
                    <span class="unit"><?php echo $kesimpulan; ?></span>
                </span>	
            </div>
        </div>
    </div>
</div>		
@endsection

@section('js')
    <script src="{{asset('js/counter.js')}}"></script>
@endsection