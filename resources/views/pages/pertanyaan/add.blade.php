@extends('layout.master')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Tambah Data Pertanyaan</h2>
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="{{route('pertanyaan.insert')}}">
                @csrf
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Pertanyaan : </label>
                        <div class="controls">
                            <textarea class="input-xlarge" id="textarea2" cols="90" rows="3" name="pertanyaan"></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Dimensi : </label>
                        <div class="controls">
                            <select id="id_dimensi" data-rel="chosen" name="id_dimensi">
                                <option></option>
                                @foreach($dimensions as $dimension)
                                <option value="{{$dimension->id_dimensi}}">{{$dimension->dimensi}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Variabel : </label>
                        <div class="controls">
                        <select id="variabel" data-rel="chosen" name="variabel">
                            <option></option>
                            <option value='komitmen pimpinan'>Komitmen pimpinan</option>
                            <option value='alokasi sumber daya'>Alokasi sumber daya</option>
                            <option value='unit pengelola teknologi'>Unit pengelola teknologi</option>
                            <option value='kebijakan dan sistem insentif'>Kebijakan dan sistem insentif</option>
                            <option value='Renstra dan peta jalan'>Renstra dan peta jalan</option>
                            <option value='Perencanaan dan pengorganisasian'>Perencanaan dan pengorganisasian</option>
                            <option value='Pengadaan dan penerapan'>Pengadaan dan penerapan</option>
                            <option value='Pengelolaan dan pengembangan'>Pengelolaan dan pengembangan</option>
                            <option value='Pemantauan dan penilaian'>Pemantauan dan penilaian</option>
                            <option value='Dosen dan peneliti'>Dosen dan peneliti</option>
                            <option value='Mahasiswa, unsur pemilik dan pimpinan'>Mahasiswa, unsur pemilik dan pimpinan</option>
                            <option value='Manajemen, staf dan karyawan'>Manajemen, staf dan karyawan</option>
                            <option value='Peningkatan kualitas'>Peningkatan kualitas</option>
                            <option value='Efektivitas dan efisiensi'>Efektivitas dan efisiensi</option>
                            <option value='Transparansi manajemen'>Transparansi manajemen</option>
                            <option value='Utilitas sumber daya'>Utilitas sumber daya</option>
                            <option value='Transformasi organisasi'>Transformasi organisasi</option>
                            <option value='Implementasi e-learning '>Implementasi e-learning </option>
                            <option value='Berbagai sumber daya'>Berbagai sumber daya</option>
                            <option value='Pendidikan terbuka'>Pendidikan terbuka </option>
                            <option value='Pangkalan data terpadu'>Pangkalan data terpadu</option>
                            <option value='Jejaring internasiona'>Jejaring internasiona</option>

                        </select>
                        </div>

                    </div>

                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban A : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pila" type="text" value="" name="pila">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban B : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pilb" type="text" value="" name="pilb">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban C : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pilc" type="text" value="" name="pilc">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban D : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pild" type="text" value="" name="pild">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban E : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pile" type="text" value="" name="pile">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
                        <button class="btn">Cancel</button>
                    </div>
                </fieldset>
              </form>
        </div>
    </div><!--/span-->

</div>
@endsection