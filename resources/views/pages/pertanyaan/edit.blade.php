@extends('layout.master')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Edit Data Pertanyaan</h2>
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="{{route('pertanyaan.update', $pertanyaan->id_kuesioner)}}">
                @csrf
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Pertanyaan : </label>
                        <div class="controls">
                            <textarea class="input-xlarge" id="textarea2" cols="90" rows="3" name="pertanyaan">{{$pertanyaan->pertanyaan}}</textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Dimensi : </label>
                        <div class="controls">
                            <select id="id_dimensi" data-rel="chosen" name="id_dimensi">
                                <option></option>
                                @foreach($dimensions as $dimension)
                                <option {{$pertanyaan->id_dimensi == $dimension->id_dimensi ? 'selected' : ''}} value="{{$dimension->id_dimensi}}">{{$dimension->dimensi}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Variabel : </label>
                        <div class="controls">
                            <select id="variabel" data-rel="chosen" name="variabel">
                                <option></option>
                                <option {{$pertanyaan->variabel == 'komitmen pimpinan' ? 'selected' : ''}} value='komitmen pimpinan'>Komitmen pimpinan</option>
                                <option {{$pertanyaan->variabel == 'alokasi sumber daya' ? 'selected' : ''}} value='alokasi sumber daya'>Alokasi sumber daya</option>
                                <option {{$pertanyaan->variabel == 'unit pengelola teknologi' ? 'selected' : ''}} value='unit pengelola teknologi'>Unit pengelola teknologi</option>
                                <option {{$pertanyaan->variabel == 'kebijakan dan sistem insentif' ? 'selected' : ''}} value='kebijakan dan sistem insentif'>Kebijakan dan sistem insentif</option>
                                <option {{$pertanyaan->variabel == 'Renstra dan peta jalan' ? 'selected' : ''}} value='Renstra dan peta jalan'>Renstra dan peta jalan</option>
                                <option {{$pertanyaan->variabel == 'Perencanaan dan pengorganisasian' ? 'selected' : ''}} value='Perencanaan dan pengorganisasian'>Perencanaan dan pengorganisasian</option>
                                <option {{$pertanyaan->variabel == 'Pengadaan dan penerapan' ? 'selected' : ''}} value='Pengadaan dan penerapan'>Pengadaan dan penerapan</option>
                                <option {{$pertanyaan->variabel == 'Pengelolaan dan pengembangan' ? 'selected' : ''}} value='Pengelolaan dan pengembangan'>Pengelolaan dan pengembangan</option>
                                <option {{$pertanyaan->variabel == 'Pemantauan dan penilaian' ? 'selected' : ''}} value='Pemantauan dan penilaian'>Pemantauan dan penilaian</option>
                                <option {{$pertanyaan->variabel == 'Dosen dan peneliti' ? 'selected' : ''}} value='Dosen dan peneliti'>Dosen dan peneliti</option>
                                <option {{$pertanyaan->variabel == 'Mahasiswa, unsur pemilik dan pimpinan' ? 'selected' : ''}} value='Mahasiswa, unsur pemilik dan pimpinan'>Mahasiswa, unsur pemilik dan pimpinan</option>
                                <option {{$pertanyaan->variabel == 'Manajemen, staf dan karyawan' ? 'selected' : ''}} value='Manajemen, staf dan karyawan'>Manajemen, staf dan karyawan</option>
                                <option {{$pertanyaan->variabel == 'Peningkatan kualitas' ? 'selected' : ''}} value='Peningkatan kualitas'>Peningkatan kualitas</option>
                                <option {{$pertanyaan->variabel == 'Efektivitas dan efisiensi' ? 'selected' : ''}} value='Efektivitas dan efisiensi'>Efektivitas dan efisiensi</option>
                                <option {{$pertanyaan->variabel == 'Transparansi manajemen' ? 'selected' : ''}} value='Transparansi manajemen'>Transparansi manajemen</option>
                                <option {{$pertanyaan->variabel == 'Utilitas sumber daya' ? 'selected' : ''}} value='Utilitas sumber daya'>Utilitas sumber daya</option>
                                <option {{$pertanyaan->variabel == 'Transformasi organisasi' ? 'selected' : ''}} value='Transformasi organisasi'>Transformasi organisasi</option>
                                <option {{$pertanyaan->variabel == 'Implementasi e-learning' ? 'selected' : ''}} value='Implementasi e-learning '>Implementasi e-learning </option>
                                <option {{$pertanyaan->variabel == 'Berbagai sumber daya' ? 'selected' : ''}} value='Berbagai sumber daya'>Berbagai sumber daya</option>
                                <option {{$pertanyaan->variabel == 'Pendidikan terbuka' ? 'selected' : ''}} value='Pendidikan terbuka'>Pendidikan terbuka </option>
                                <option {{$pertanyaan->variabel == 'Pangkalan data terpadu' ? 'selected' : ''}} value='Pangkalan data terpadu'>Pangkalan data terpadu</option>
                                <option {{$pertanyaan->variabel == 'Jejaring internasiona' ? 'selected' : ''}} value='Jejaring internasiona'>Jejaring internasiona</option>
                            </select>
                        </div>

                    </div>

                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban A : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pila" type="text" value="{{$pertanyaan->pila}}" name="pila">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban B : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pilb" type="text" value="{{$pertanyaan->pilb}}" name="pilb">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban C : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pilc" type="text" value="{{$pertanyaan->pilc}}" name="pilc">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban D : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pild" type="text" value="{{$pertanyaan->pild}}" name="pild">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="focusedInput">Jawaban E : </label>
                        <div class="controls">
                        <input class="input-xlarge focused" id="pile" type="text" value="{{$pertanyaan->pile}}" name="pile">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
                        <button class="btn">Cancel</button>
                    </div>
                </fieldset>
              </form>
        </div>
    </div><!--/span-->

</div>
@endsection