@extends('layout.master')
@section('content')
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white user"></i><span class="break"></span>Pertanyaan</h2>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>No.</th>
					  <th>Pertanyaan</th>
					  <th>Variabel</th>
					  <th>Actions</th>
				  </tr>
			  </thead>   
			  <tbody>
				@foreach($questions as $question)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$question->pertanyaan}}</td>
					<td class="center">{{$question->variabel}}</td>
					
					<td class="center">
						<a class="btn btn-info" href="{{route('pertanyaan.edit', $question->id_kuesioner)}}">
							<i class="halflings-icon white edit"></i>  
						</a>
						<a class="btn btn-danger" href="{{route('pertanyaan.delete', $question->id_kuesioner)}}">
							<i class="halflings-icon white trash"></i> 
						</a>
					</td>
				</tr>
				@endforeach
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->

</div>
@endsection