@extends('layout.master')
@section('content')
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white user"></i><span class="break"></span>Pertanyaan</h2>
		</div>
		<div class="box-content">
      {{-- @php
      dd(session('submittedData')[2])
      @endphp --}}
                    <form name="form1" action="{{route('questionaire.submit')}}" method="post">
                      @csrf
                    @foreach($questions as $question)
                    <input type="hidden" name="id[{{$question->id_kuesioner}}]" value="{{$question->id_kuesioner}}">
                    <input type="hidden" name="jumlah" value="{{count($questions)}}">
                     <div class="control-group">
                            <label class="control-label">{{$loop->iteration}}. {{$question->pertanyaan}}
                          <td></label>
                            <div class="controls">
                              <label class="radio">
                                <input type="radio" {{ isset(session('submittedData')[$question->id_kuesioner]) && session('submittedData')[$question->id_kuesioner] == 'A' ? 'checked' : '' }} name="pilihan[{{$question->id_kuesioner}}]" value="A" >A. {{$question->pila}}
                                    <br>
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" {{ isset(session('submittedData')[$question->id_kuesioner]) && session('submittedData')[$question->id_kuesioner] == 'B' ? 'checked' : '' }} name="pilihan[{{$question->id_kuesioner}}]" value="B">B. {{$question->pilb}}
                                </label>
                                <div style="clear:both"></div>
                                
                                <label class="radio">
                                 <input type="radio" {{ isset(session('submittedData')[$question->id_kuesioner]) && session('submittedData')[$question->id_kuesioner] == 'C' ? 'checked' : '' }} name="pilihan[{{$question->id_kuesioner}}]" value="C">C. {{$question->pilc}}
                                 </label>
                                 <div style="clear:both"></div>
                                 
                                 <label class="radio">
                                 <input type="radio" {{ isset(session('submittedData')[$question->id_kuesioner]) && session('submittedData')[$question->id_kuesioner] == 'D' ? 'checked' : '' }} name="pilihan[{{$question->id_kuesioner}}]" value="D">D. {{$question->pild}}
                                 </label>
                                 <div style="clear:both"></div>
                                 
                                 <label class="radio">
                                 <input type="radio" {{ isset(session('submittedData')[$question->id_kuesioner]) && session('submittedData')[$question->id_kuesioner] == 'E' ? 'checked' : '' }} name="pilihan[{{$question->id_kuesioner}}]" value="E">E. {{$question->pile}}
                                  </label>

                            </div>
                          </div>

                    @endforeach
                    <tr>
                        <td>&nbsp;</td>
                          <td><input type="submit" name="submit" value="Jawab" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')"></td>
                    </tr>
                    </form>
            </div>
</div>
@endsection