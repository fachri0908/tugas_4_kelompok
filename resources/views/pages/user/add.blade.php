@extends('layout.master')
@section('content')
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white edit"></i><span class="break"></span>Tambah Data User</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="POST" action="{{route('user.insert')}}">
					@csrf
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="focusedInput">Username : </label>
							<div class="controls">
								<input class="input-xlarge focused" id="focusedInput" type="text" value="" name="username">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="focusedInput">Password : </label>
							<div class="controls">
								<input class="input-xlarge focused" id="" type="password" value="" name="password">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="focusedInput">Hak Akses : </label>
							<div class="controls">
								<select id="hak_akses" data-rel="chosen" name="hak_akses">
									<option></option>
									<option value="dekan">Dekan</option>
									<option value="kepala puskom">Kepala Puskom</option>
									<option value="kaprodi ti">Kaprodi TI</option>
									<option value="kaprodi si">Kaprodi SI</option>
								</select>
							</div>
						</div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
							<button class="btn">Cancel</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div><!--/span-->
	</div>
@endsection