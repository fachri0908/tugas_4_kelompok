@extends('layout.master')
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Edit Data User</h2>
		</div>
		<div class="box-content">
			<form class="form-horizontal" method="POST" action="{{route('user.update', $selectedUser->user_id)}}">
				@csrf
				<fieldset>
					<div class="control-group">
						<label class="control-label" for="focusedInput">Username : </label>
						<div class="controls">
							<input class="input-xlarge focused" id="focusedInput" type="text" value="{{$selectedUser->username}}" name="username">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="focusedInput">Password : </label>
						<div class="controls">
							<input class="input-xlarge focused" id="" type="password" value="{{$selectedUser->password}}" name="password">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="focusedInput">Hak Akses : </label>
						<div class="controls">
							<select id="hak_akses" data-rel="chosen" name="hak_akses">
								<option></option>
								<option {{$selectedUser->hak_akses =='dekan' ? 'selected' : ''}} value="dekan">Dekan</option>
								<option {{$selectedUser->hak_akses =='kepala puskom' ? 'selected' : ''}} value="kepala puskom">Kepala Puskom</option>
								<option {{$selectedUser->hak_akses =='kaprodi ti' ? 'selected' : ''}} value="kaprodi ti">Kaprodi TI</option>
								<option {{$selectedUser->hak_akses =='kaprodi si' ? 'selected' : ''}} value="kaprodi si">Kaprodi SI</option>
							</select>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
						<button class="btn">Cancel</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div><!--/span-->
</div><!--/row-->
@endsection