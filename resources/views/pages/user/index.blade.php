@extends('layout.master')
@section('content')
	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white user"></i><span class="break"></span>Dimensi</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Username</th>
						<th>Hak Akses</th>
						<th>Actions</th>
					</tr>
				</thead>   
				<tbody>
					@foreach($users as $userlist)
					<tr>
						<td class="center">{{$loop->iteration}}</td>
						<td>{{$userlist->username}}</td>
						<td class="center">{{$userlist->hak_akses}}</td>
						
						<td class="center">
							<a class="btn btn-info" href="{{route('user.edit', $userlist->user_id)}}">
								<i class="halflings-icon white edit"></i>  
							</a>
							<a class="btn btn-danger" href="{{route('user.delete', $userlist->user_id)}}">
								<i class="halflings-icon white trash"></i> 
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>            
			</div>
		</div>
	</div>
@endsection