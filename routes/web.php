<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DimensiController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\QuestionaireController;
use App\Http\Controllers\UserController;

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('authenticate', [AuthController::class, 'authenticate'])->name('authenticate'); 
Route::get('/signout', [AuthController::class, 'signOut'])->name('signout');
Route::get('/', [DashboardController::class, 'index'])->middleware('auth')->name('dashboard');

Route::get('/dimensi', [DimensiController::class, 'index'])->middleware('auth')->name('dimensi.index');
Route::get('/dimensi/add', [DimensiController::class, 'add'])->middleware('auth')->name('dimensi.add');
Route::post('/dimensi/insert', [DimensiController::class, 'insert'])->middleware('auth')->name('dimensi.insert');
Route::get('/dimensi/delete/{id_dimensi}', [DimensiController::class, 'delete'])->middleware('auth')->name('dimensi.delete');
Route::get('/dimensi/edit/{id_dimensi}', [DimensiController::class, 'edit'])->middleware('auth')->name('dimensi.edit');
Route::post('/dimensi/update/{id_dimensi}', [DimensiController::class, 'update'])->middleware('auth')->name('dimensi.update');

Route::get('/pertanyaan', [PertanyaanController::class, 'index'])->middleware('auth')->name('pertanyaan.index');
Route::get('/pertanyaan/add', [PertanyaanController::class, 'add'])->middleware('auth')->name('pertanyaan.add');
Route::post('/pertanyaan/insert', [PertanyaanController::class, 'insert'])->middleware('auth')->name('pertanyaan.insert');
Route::get('/pertanyaan/delete/{id_pertanyaan}', [PertanyaanController::class, 'delete'])->middleware('auth')->name('pertanyaan.delete');
Route::get('/pertanyaan/edit/{id_pertanyaan}', [PertanyaanController::class, 'edit'])->middleware('auth')->name('pertanyaan.edit');
Route::post('/pertanyaan/update/{id_pertanyaan}', [PertanyaanController::class, 'update'])->middleware('auth')->name('pertanyaan.update');

Route::get('/questionaire', [QuestionaireController::class, 'index'])->middleware('auth')->name('questionaire.index');
Route::post('/questionaire/submit', [QuestionaireController::class, 'submitQuestionaire'])->middleware('auth')->name('questionaire.submit');

Route::get('/user', [UserController::class, 'index'])->middleware('auth')->name('user.index');
Route::get('/user/add', [UserController::class, 'add'])->middleware('auth')->name('user.add');
Route::post('/user/insert', [UserController::class, 'insert'])->middleware('auth')->name('user.insert');
Route::get('/user/delete/{id_user}', [UserController::class, 'delete'])->middleware('auth')->name('user.delete');
Route::get('/user/edit/{id_user}', [UserController::class, 'edit'])->middleware('auth')->name('user.edit');
Route::post('/user/update/{id_user}', [UserController::class, 'update'])->middleware('auth')->name('user.update');